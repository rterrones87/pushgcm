<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PushNotificationForAndroid extends CI_Controller {

	public function index(){

		// $input = '{
		// 			"apps" : {
		// 						"alert":"",
		// 						"badge":"1",
		// 						"sound":"",
		// 						"content-available":"1"
		// 					},
		// 			"Terminals":[
  //     								{
  //        								"Name":"TNG",
  //        								"Permissions": ["mp001","mp002","mp003","mp004","mp005","mp007","mp008","mp009","mp010","mp011"]
  //     								}
  //  								],
  //  					"User":"rterrones@ehecatl.com.mx"
		// 		}';

		// $results = json_decode($input);
		// echo "<br />Error : " . json_last_error(); // 4 (JSON_ERROR_SYNTAX)
		// echo "<br />Mensaje : " . json_last_error_msg(); // unexpected character
		$this->load->view("index");

	}

	public function send_gcm(){

		// AIzaSyARLtoj70ponE19vQbDvniCjCEMkrXlFi0
	        
		$response = "";
	    $this->load->library('gcm.php');

	    $apikey = $this->input->post("api_key");
	    $device_push = $this->input->post("device_push");
	    $payload = $this->input->post("payload");

        $this->gcm->addRecepient( $device_push );
        $this->gcm->setData( json_decode($payload) );
        $this->gcm->setTtl(500);
        $this->gcm->setTtl(false);
        $this->gcm->setGroup(false);

        if ( json_last_error() != 0 ){
        	$response = array( "error" => "1" , "message" => json_last_error_msg() );
        }else{
        	$this->gcm->send( $apikey );
        	$response = $this->gcm->status; 
        }

        $this->output->set_header('Content-type: application/json; charset=UTF-8');
		$this->output->set_output(json_encode( $response , JSON_NUMERIC_CHECK));

	}
}
