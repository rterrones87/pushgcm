<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	
	public function index()
	{
		$this->load->view("index");
	}

	public function ios()
	{
		$this->load->view("ios");
	}

	public function send_gcm(){

		// AIzaSyARLtoj70ponE19vQbDvniCjCEMkrXlFi0
	        
		$response = "";
	    $this->load->library('gcm');

	    $apikey = $this->input->post("api_key");
	    $device_push = $this->input->post("device_push");
	    $payload = $this->input->post("payload");

        $this->gcm->addRecepient( $device_push );
        $this->gcm->setData( json_decode($payload) );
        $this->gcm->setTtl(500);
        $this->gcm->setTtl(false);
        $this->gcm->setGroup(false);

        if ( json_last_error() != 0 ){
        	$response = array( "error" => "1" , "message" => json_last_error_msg() );
        }else{
        	$this->gcm->send( $apikey );
        	$response = $this->gcm->status; 
        }

        $this->output->set_header('Content-type: application/json; charset=UTF-8');
		$this->output->set_output(json_encode( $response , JSON_NUMERIC_CHECK));

	}


	public function pushios()
	{
		//echo FCPATH ."mp_cer.pem";
		$apnsHost = 'gateway.sandbox.push.apple.com';
		$apnsCert = FCPATH ."mp_cer.pem";
		
		$apnsPort = 2195;
		$apnsPass = '';
		$token = $this->input->post("device_push");
		$payload = $this->input->post("payload");

		$payload = json_decode($payload);
		$output = json_encode($payload);
		$token = pack('H*', str_replace(' ', '', $token));
		$apnsMessage = chr(0).chr(0).chr(32).$token.chr(0).chr(strlen($output)).$output;

		$streamContext = stream_context_create();
		stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
		stream_context_set_option($streamContext, 'ssl', 'passphrase', $apnsPass);

		$apns = stream_socket_client('ssl://'.$apnsHost.':'.$apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $streamContext);

		

		if ( json_last_error() != 0 ){
        	$response = array( "error" => "1" , "message" => json_last_error_msg() );
        }else{
        	$response = fwrite($apns, $apnsMessage);
        	if (!$response)
		    	$response = array( "error" => "0" , "message" => "Message not delivered" );
			else
				$response = array( "error" => "0" , "message" => "Message successfully delivered" );
        }

        fclose($apns);
        $this->output->set_header('Content-type: application/json; charset=UTF-8');
		$this->output->set_output(json_encode( $response , JSON_NUMERIC_CHECK));




		



		
	}

}
