<?php $this->load->view("header.php", array("device" => "android")); ?>
  <script type="text/javascript">
    $(document).ready(function(){
      // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
      

      var base_url = "<?php echo base_url(); ?>";
      $('.modal-trigger').leanModal( {dismissible: false} );


      $(".modal-trigger").click(function(){

          $('selector').click(function(){return false;});  
          $(".progress").show();
          $("#response_msg").text( "Sending..." );


          $.ajax({
            type: "POST",
            url:  "send_gcm",
            data: $("#formulario").serialize(), // serializes the form's elements.
            success: function(data)
            {
              $(".progress").hide();
              $("#response_msg").text( data.message );
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) { 
                $(".progress").hide();
                $("#response_msg").text( "Sorry, something wrong has happened =(" );
            } 
          });
      });


    });
  </script>


<!-- Modal Structure -->
<div id="mda" class="modal bottom-sheet">
  <div class="modal-content">
    <div class="progress">
      <div class="indeterminate"></div>
    </div>
    <h5 id='response_msg'></h5>
  </div>
  <div class="modal-footer">
    <a href="javascript:;" class=" modal-action modal-close waves-effect waves-green btn-flat">Continuar!</a>
  </div>
  </div>

 <div class="row">
    <form id="formulario" class="col s12" method="POST"> 
      <div class="row">
        <div class="input-field col s6">
          <input id="api_key" type="text" name="api_key" class="validate">
          <label for="api_key">API KEY</label>
        </div>
        <div class="input-field col s6">
          <input id="device_push" name="device_push" type="text" class="validate">
          <label for="device_push">DEVICE PUSH KEY</label>
        </div>

        <div class="input-field col s12">
         <textarea placeholder="" name="payload" id="address" class="materialize-textarea validate" rows="80"></textarea>
                  <label for="address">PAYLOAD</label>
        </div>

        <div class="input-field col s12 center" >
          <a class="waves-effect waves-light btn modal-trigger" href="#mda">Enviar Push <i class="material-icons right">send</i></a>
          
        </div>
      </div>
    </form>
  </div>

  <?php $this->load->view("footer.php"); ?>